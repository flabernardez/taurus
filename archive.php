<?php
/**
 * The template for displaying archive pages
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Taurus_theme
 */

get_header();

if ( is_post_type_archive( 'criticas')) {
	get_sidebar();
}

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">

		<?php
		if ( have_posts() ) : ?>

			<?php
			/* Start the Loop */
			while ( have_posts() ) : the_post();

                    get_template_part( 'template-parts/content', 'archive' );

			endwhile;

                if ( !is_post_type_archive('criticas') ) :

	                misha_paginator( get_pagenum_link() );

                endif;

            else :

			get_template_part( 'template-parts/content', 'none' );

		endif;
		?>

        </main><!-- #main -->


	</div><!-- #primary -->



<?php
get_footer();