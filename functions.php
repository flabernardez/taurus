<?php
/**
 * Taurus theme functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Taurus_theme
 */

if ( ! function_exists( 'taurus_theme_setup' ) ) :

	function taurus_theme_setup() {

		load_theme_textdomain( 'taurus-theme', get_template_directory() . '/languages' );

		add_theme_support( 'automatic-feed-links' );

		add_theme_support( 'title-tag' );

		add_theme_support( 'post-thumbnails' );

		add_filter('jpeg_quality', function($arg){return 100;});

		register_nav_menus( array(
			'menu-1' => esc_html__( 'Primary', 'taurus-theme' ),
		) );

		register_nav_menus( array(
			'menu-2' => esc_html__( 'Footer', 'taurus-theme' ),
		) );

		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		add_theme_support( 'custom-background', apply_filters( 'taurus_theme_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		add_theme_support( 'customize-selective-refresh-widgets' );

		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );
	}
endif;
add_action( 'after_setup_theme', 'taurus_theme_setup' );

function taurus_theme_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'taurus_theme_content_width', 640 );
}
add_action( 'after_setup_theme', 'taurus_theme_content_width', 0 );

function taurus_theme_widgets_init() {
	register_sidebar( array(
		'name'          => esc_html__( 'Sidebar', 'taurus-theme' ),
		'id'            => 'sidebar-1',
		'description'   => esc_html__( 'Add widgets here.', 'taurus-theme' ),
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2 class="widget-title">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => esc_html__( 'Home Hero', 'taurus-theme' ),
		'id'            => 'hero-home',
		'description'   => esc_html__( 'Add widgets here.', 'taurus-theme' ),
		'before_widget' => '<section id="%1$s" class="hero">',
		'after_widget'  => '</section>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'taurus_theme_widgets_init' );

function taurus_theme_scripts() {
	wp_enqueue_style( 'taurus-theme-style', get_stylesheet_uri() );

	wp_enqueue_script( 'taurus-theme-navigation', get_template_directory_uri() . '/js/navigation-min.js', array(), '20151215', true );
	wp_enqueue_script( 'taurus-theme-skip-link-focus-fix', get_template_directory_uri() . '/js/skip-link-focus-fix-min.js', array(), '20151215', true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'taurus_theme_scripts', 99 );

require get_template_directory() . '/inc/custom-header.php';

require get_template_directory() . '/inc/template-tags.php';

require get_template_directory() . '/inc/template-functions.php';

require get_template_directory() . '/inc/customizer.php';

if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

add_post_type_support( 'page', 'excerpt' );

function remove_hentry( $classes ) {
    $classes = array_diff($classes, array('hentry'));
    return $classes;
}
add_filter( 'post_class', 'remove_hentry' );

/* Load more FACET button */
function fwp_load_more() {
	?>
    <script>
        (function($) {
            $(function() {
                if ('object' != typeof FWP) {
                    return;
                }

                FWP.hooks.addFilter('facetwp/template_html', function(resp, params) {
                    if (FWP.is_load_more) {
                        FWP.is_load_more = false;
                        $('.facetwp-template').append(params.html);
                        return true;
                    }
                    return resp;
                });
            });

            $(document).on('click', '.fwp-load-more', function() {
                $('.fwp-load-more').html('Cargando...');
                FWP.is_load_more = true;
                FWP.paged = parseInt(FWP.settings.pager.page) + 1;
                FWP.soft_refresh = true;
                FWP.refresh();
            });

            $(document).on('facetwp-loaded', function() {
                if (FWP.settings.pager.page < FWP.settings.pager.total_pages) {
                    if (! FWP.loaded && 1 > $('.fwp-load-more').length) {
                        $('.facetwp-template').after('<button class="fwp-load-more">Cargar más</button>');
                    }
                    else {
                        $('.fwp-load-more').html('Cargar más').show();
                    }
                }
                else {
                    $('.fwp-load-more').hide();
                }
            });

            $(document).on('facetwp-refresh', function() {
                if (! FWP.loaded) {
                    FWP.paged = 1;
                }
            });
        })(jQuery);
    </script>
	<?php
}
add_action( 'wp_head', 'fwp_load_more', 99 );
add_filter( 'facetwp_template_force_load', '__return_true' );

/* Pagination Misha */
function misha_paginator( $first_page_url ){

	global $wp_query;

	$first_page_url = untrailingslashit( $first_page_url );

    $first_page_url_exploded = array(); // set it to empty array
	$first_page_url_exploded = explode("/?", $first_page_url);
	$search_query = '';
	if( isset( $first_page_url_exploded[1] ) ) {
		$search_query = "/?" . $first_page_url_exploded[1];
		$first_page_url = $first_page_url_exploded[0];
	}

	$posts_per_page = (int) $wp_query->query_vars['posts_per_page'];
	$current_page = (int) $wp_query->query_vars['paged'];
	$max_page = $wp_query->max_num_pages;

	if( $max_page <= 1 ) return;

	if( empty( $current_page ) || $current_page == 0) $current_page = 1;

	$links_in_the_middle = 4;
	$links_in_the_middle_minus_1 = $links_in_the_middle-1;

	$first_link_in_the_middle = $current_page - floor( $links_in_the_middle_minus_1/2 );
	$last_link_in_the_middle = $current_page + ceil( $links_in_the_middle_minus_1/2 );

	if( $first_link_in_the_middle <= 0 ) $first_link_in_the_middle = 1;
	if( ( $last_link_in_the_middle - $first_link_in_the_middle ) != $links_in_the_middle_minus_1 ) { $last_link_in_the_middle = $first_link_in_the_middle + $links_in_the_middle_minus_1; }
	if( $last_link_in_the_middle > $max_page ) { $first_link_in_the_middle = $max_page - $links_in_the_middle_minus_1; $last_link_in_the_middle = (int) $max_page; }
	if( $first_link_in_the_middle <= 0 ) $first_link_in_the_middle = 1;

	$pagination = '<nav id="misha_pagination" class="pagination" role="navigation"><div class="nav-links">';

	if ($first_link_in_the_middle >= 3 && $links_in_the_middle < $max_page) {
		$pagination.= '<a href="'. $first_page_url . $search_query . '" class="page-numbers">1</a>';

		if( $first_link_in_the_middle != 2 )
			$pagination .= '<span class="page-numbers extend">...</span>';

	}

	// arrow left (previous page)
	if ($current_page != 1)
		$pagination.= '<a href="'. $first_page_url . '/page/' . ($current_page-1) . $search_query . '" class="prev page-numbers"></a>';


	for($i = $first_link_in_the_middle; $i <= $last_link_in_the_middle; $i++) {
		if($i == $current_page) {
			$pagination.= '<span class="page-numbers current">'.$i.'</span>';
		} else {
			$pagination .= '<a href="'. $first_page_url . '/page/' . $i . $search_query .'" class="page-numbers">'.$i.'</a>';
		}
	}

	// arrow right (next page)
	if ($current_page != $last_link_in_the_middle )
		$pagination.= '<a href="'. $first_page_url . '/page/' . ($current_page+1) . $search_query .'" class="next page-numbers"></a>';


	if ( $last_link_in_the_middle < $max_page ) {

		if( $last_link_in_the_middle != ($max_page-1) )
			$pagination .= '<span class="page-numbers extend">...</span>';

		$pagination .= '<a href="'. $first_page_url . '/page/' . $max_page . $search_query .'" class="page-numbers">'. $max_page .'</a>';
	}

	// end HTML
	$pagination.= "</div></nav>\n";

	// haha, this is our load more posts link
	if( $current_page < $max_page )
		$pagination.= '<div class="loadmore_container"><div id="misha_loadmore" class="button">Cargar más</div></div>';

	echo str_replace(array("/page/1?", "/page/1\""), array("?", "\""), $pagination);
}

/* Load more ARCHIVE button */
function misha_my_load_more_scripts() {

	global $wp_query;

	wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/js/myloadmore-min.js', array('jquery') );

	wp_localize_script( 'my_loadmore', 'misha_loadmore_params', array(
		'ajaxurl'      => site_url() . '/wp-admin/admin-ajax.php', // WordPress AJAX
		'posts'        => json_encode( $wp_query->query_vars ), // everything about your loop is here
		'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
		'max_page'     => $wp_query->max_num_pages,
		'first_page'   => get_pagenum_link(1) // here it is
	) );

	wp_enqueue_script( 'my_loadmore' );
}
add_action( 'wp_enqueue_scripts', 'misha_my_load_more_scripts' );
function misha_loadmore_ajax_handler(){

	// prepare our arguments for the query
	$args = json_decode( stripslashes( $_POST['query'] ), true );
	$args['paged'] = $_POST['page'] + 1; // we need next page to be loaded
	$args['post_status'] = 'publish';

	query_posts( $args );

	if( have_posts() ) :

		// run the loop
		while( have_posts() ): the_post();

			get_template_part( 'template-parts/content', 'archive' );

		endwhile;

		misha_paginator( $_POST['first_page'] );

	endif;
	die;
}
add_action('wp_ajax_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_{action}
add_action('wp_ajax_nopriv_loadmore', 'misha_loadmore_ajax_handler'); // wp_ajax_nopriv_{action}