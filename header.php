<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Taurus_theme
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
    
    <script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
    <script>
     (adsbygoogle = window.adsbygoogle || []).push({
          google_ad_client: "ca-pub-6276143273998710",
          enable_page_level_ads: true
     });
    </script>
    
    <!-- media.net code begin -->
    <script type="text/javascript">
        window._mNHandle = window._mNHandle || {};
        window._mNHandle.queue = window._mNHandle.queue || [];
        medianet_versionId = "3121199";
    </script>
    <script src="//contextual.media.net/dmedianet.js?cid=8CUA315N2" async="async"></script>
    <!-- media.net code fin -->    

	<?php
        if ( is_singular('criticas') ) {

		        $imdb_field = get_field('imdb');
		        $imdb = imdb_connector_get_movie($imdb_field);

		        global $wp;
	            global $post;


	        echo '<script type="application/ld+json">
                    {
                      "@context": "http://schema.org/",
                      "@type": "Review",
                      "itemReviewed": {
                        "@type": "Movie",
                        "name": "'. get_the_title() .'",
                        "sameAs": "https://www.imdb.com/title/'. $imdb_field .'/",
                        "datePublished": "'. $imdb['released'] .'",
                        "image": "'. wp_get_attachment_url( get_post_thumbnail_id($post->ID), 'thumbnail' ) .'",
                        "director": {
                            "@type": "Person",
                            "name": "'. implode(', ' , $imdb['directors'] ) .'"
                        } 
                      },
                      "datePublished": "'. get_the_date('Y/m/d') .'",
                      "description": "'. get_the_excerpt() .'",
                      "inLanguage": "es",
                      "reviewBody": "'. $post->post_content .'",
                      "author": {
                        "@type": "Person",
                        "name": "Luis López Belda",
                        "sameAs": "http://luislobelda.com/"
                      },
                      "reviewRating": {
                        "@type": "Rating",
                        "ratingValue": "'. get_field('puntuacion') .'",
                        "bestRating": "5",
                        "worstRating": "0"
                      },
                      "publisher": {
                        "@type": "Organization",
                        "name": "Alicante Mag",
                        "sameAs": "http://alicantemag.com/"
                      },
                      "URL": "'. $current_url = home_url(add_query_arg(array(),$wp->request)) .'"
                    }
                    </script>
                ';

	        }
	?>

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'taurus-theme' ); ?></a>

	<?php if ( is_post_type_archive( 'criticas' ) ) : ?>

        <?php $thumb = content_url() . '/assets/criticas-portada.jpg';?>
        <header id="masthead" class="site-header" style="
                background-image: linear-gradient(
                rgba(252, 56, 85, 0.5),
                rgba(252, 56, 85, 0.5)
                ),
                url('<?php echo $thumb;?>');
        ">

	<?php elseif ( is_post_type_archive( 'criticas-al-salir' ) ) : ?>

	        <?php $thumb = content_url() . '/assets/criticas-al-salir-portada.jpg';?>
            <header id="masthead" class="site-header" style="
                    background-image: linear-gradient(
                    rgba(252, 56, 85, 0.5),
                    rgba(252, 56, 85, 0.5)
                    ),
                    url('<?php echo $thumb;?>');
                    ">
	<?php elseif ( is_post_type_archive( 'cautivos-del-film' ) ) : ?>

	            <?php $thumb = content_url() . '/assets/cautivos-del-film-portada.jpg';?>
                <header id="masthead" class="site-header" style="
                        background-image: linear-gradient(
                        rgba(252, 56, 85, 0.5),
                        rgba(252, 56, 85, 0.5)
                        ),
                        url('<?php echo $thumb;?>');
                        ">
	<?php elseif ( is_singular( 'criticas' ) ) : ?>

        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
        <header id="masthead" class="site-header" style="
                background-image: linear-gradient(
                rgba(150, 27, 45, 0.75),
                rgba(150, 27, 45, 0.75)
                ),
                url('<?php echo $thumb['0'];?>');
                ">

    <?php else : ?>

        <?php $thumb = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );?>
        <header id="masthead" class="site-header" style="
                background-image: linear-gradient(
                rgba(252, 56, 85, 0.5),
                rgba(252, 56, 85, 0.5)
                ),
                url('<?php echo $thumb['0'];?>');
        ">

    <?php endif ; ?>

        <div class="top-bar wrapper">
            <div class="site-branding">
		        <?php
		        the_custom_logo();
		        if ( is_front_page() && is_home() ) : ?>
                    <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
		        <?php else : ?>
                    <p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			        <?php
		        endif;

		        $description = get_bloginfo( 'description', 'display' );
		        if ( $description || is_customize_preview() ) : ?>
                    <p class="site-description"><?php echo $description; /* WPCS: xss ok. */ ?></p>
			        <?php
		        endif; ?>
            </div><!-- .site-branding -->
            <nav id="site-navigation" class="main-navigation">
                <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php esc_html_e( 'Menu', 'taurus-theme' ); ?></button>
		        <?php
		        wp_nav_menu( array(
			        'theme_location' => 'menu-1',
			        'menu_id'        => 'primary-menu',
		        ) );
		        ?>
            </nav><!-- #site-navigation -->
        </div>
        <div class="wrapper">

            <?php
                if ( is_front_page() ) :

                    dynamic_sidebar( 'hero-home' );

                elseif ( is_post_type_archive( 'criticas' ) ) : ?>

                    <section id="hero-archive" class="hero">
                        <h1>Críticas</h1>
                        <p>Un canto de amor al cine, esas imágenes proyectadas, ese esplendor en la oscuridad que tantas veces nos salva de un mundo en quiebra.</p>
                    </section>

                <?php elseif ( is_archive() ) : ?>

                    <section id="hero-archive" class="hero">
                        <h1><?php the_archive_title() ?></h1>
                        <p><?php the_archive_description() ?></p>
                    </section>

                <?php elseif ( is_post_type_archive( 'criticas-al-salir' ) ) : ?>

                    <section id="hero-archive" class="hero">
                        <h1>Críticas al salir</h1>
                        <p>Mini episodios donde Luis López Belda habla de las películas que acaba de ver en el cine. A veces solo, otras acompañado, graba con el móvil las impresiones del film nada más salir.</p>
                    </section>

                <?php elseif ( is_post_type_archive( 'cautivos-del-film' ) ) : ?>

                    <section id="hero-archive" class="hero">
                        <h1>Cautivos del film</h1>
                        <p>Un viaje a través del cine en el que charlamos sobre las emociones humanas, sus problemáticas, contradicciones, logros y superaciones compartiendo con vosotros las películas que vamos viendo.</p>
                    </section>

                <?php elseif ( is_singular( 'criticas' ) ) : ?>

                    <section id="hero-archive" class="hero">
                        <h1><?php the_title(); ?></h1>
                        <div class="hero-criticas">
	                        <?php

	                        $term_list = wp_get_post_terms($post->ID, 'tipo', array("fields" => "all"));

	                        foreach($term_list as $term_single) {

		                        echo '<a href="' .get_term_link($term_single). '" class="category-button">' . $term_single->name . '</a>';

                            };

	                        ?>
	                        <span class="the-date"><?php echo get_the_date(); ?></span>
                        </div>
                    </section>

            <?php else : ?>

                <section id="hero-singular" class="hero">
                    <h1><?php the_title(); ?></h1>
                </section>

            <?php endif; ?>

        </div>

	</header><!-- #masthead -->

	<div id="content" class="site-content wrapper">
