<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Taurus_theme
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
        <div class="footer-container wrapper">
            <div class="site-info">
                <p>
			        <?php printf( esc_html__( 'Web realizada por %1$s desde Alacant con mucho amor.', 'taurus-theme' ), '<a href="https://flaviabernardez.com/" target="_blank" alt="web Flavia">Flavia Bernárdez</a>' ); ?>
                </p>
            </div><!-- .site-info -->
            <nav class="footer-navigation">
		        <?php
		        wp_nav_menu( array(
			        'theme_location' => 'menu-2',
			        'menu_id'        => 'footer-menu',
		        ) );
		        ?>
            </nav>
        </div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
