<?php
/**
 * Template part for displaying críticas
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Taurus_theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">

		<div class="entry-excerpt">
			<?php the_excerpt(); ?>
		</div><!-- .entry-meta -->

	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php

		the_content( sprintf(
			wp_kses(
				__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'taurus-theme' ),
				array(
					'span' => array(
						'class' => array(),
					),
				)
			),
			get_the_title()
		) );

		wp_link_pages( array(
			'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'taurus-theme' ),
			'after'  => '</div>',
		) );
		?>
	</div><!-- .entry-content -->

<?php if( get_field('peliculas') ): ?>

    <aside class="ficha">

        <div class="ficha-wrapper">

            <?php if( have_rows('peliculas') ): ?>

                <ul class="lista">


                    <?php
                    /** Make sure we get the right rows */
                    the_row();

                    /** We read the name of the object containing the repeater field */
                    $parent_field_name = "peliculas";

                    /** Now we get all its values */
                    $parent_field_value = get_field( "peliculas" );

                    /** We count the values */
                    $parent_field_count = count( $parent_field_value );

                    /** We only want to display anything if there's at least one movie entered in the row */
                    if ( $parent_field_count ) {

                        /** Now we start to go through the rows */
                        foreach ( $parent_field_value as $field => $value ) {
                            /** We use the IMDb ID as default selector because it's more precise */
                            $selector = $value["imdb_id"];

                            /** If there's no IMDb ID entered, we use the Spanish title */
                            if ( ! $selector ) {
                                $selector = $value["titulo_spain"];
                            }

                            /** Now we get all the details from the current movie */
                            $movie_details = imdb_connector_get_movie( $selector );

                            /** If it fails for some reason, we stop and add an error message to the debug.log file */
                            if ( ! $movie_details ) {
                                error_log( "The entered movie title or IMDb ID did not return any movie." );

                                break;
                            }
                            ?>
                            <!-- This is how we display the values in HTML -->
                                <li>
                                    <img class="cartel" src="<?php echo $movie_details['poster'] ?>"/>
                                    <span class="datos">
                                        <a class="titulo" href="<?php echo $value["enlace"]; ?>">
                                            <?php echo $value["titulo_spain"]; ?>
                                        </a><br/>
                                        <span class="director">
                                            <?php echo implode(', ' , $movie_details['directors'] ); ?>
                                        </span>
                                    </span>
                                </li>
                            <?php
                        }
                    }
                    ?>

                </ul>

            <?php endif; ?>

        </div>

    </aside>

<?php endif; ?>

<?php if( get_field('imdb') ) : ?>

    <aside class="ficha">

        <div class="ficha-wrapper">

            <h3>Ficha artística y sinopsis</h3>
            <?php

                $movie_field = get_field('imdb');
                $movie = imdb_connector_get_movie($movie_field);

                if ( $movie_field ) {

                        echo '<ul class="datos">';
                        echo '<li><strong>Título original:</strong> ' . $movie['title'] . '</li>';
                        echo '<li><strong>Año:</strong>             ' . $movie['year']  . '</li>';
                        echo '<li><strong>Duración:</strong>        ' . $movie["runtime"]["minutes"]  . ' minutos</li>';
                        echo '<li><strong>País:</strong>            ' . implode(', ' , $movie['countries'] )  . '</li>';
                        echo '<li><strong>Director:</strong>        ' . implode(', ' , $movie['directors'] )   . '</li>';
                        echo '<li><strong>Actores:</strong>         ' . implode(', ' , $movie['actors'] )   . '</li>';
                        echo '<li><strong>Guión:</strong>           ' . implode(', ' , $movie['writers'] )   . '</li>';
                        echo '<li><strong>Género:</strong>          ' . implode(', ' , $movie['genres'] )    . '</li>';
                        echo '</ul>';

                        echo '<img class="peli" src="' . $movie['poster'] . '"/>';

                        echo '<p class="sinopsis">' . get_field('sinopsis') . '</p>';
                }
            ?>
        </div>

    </aside>

    <aside class="ficha conclusion">

            <div class="ficha-wrapper">

            <?php
            echo '<h3>Conclusión final y valoración</h3>';

            echo '<p class="conclusion">' . get_field('conclusion') . '</p>';

            echo '<div class="puntuacion">';

                echo '<h4>Total</h4>';

                $output = get_field('puntuacion');
                $nota = number_format( $output, 1, ',','.');

                echo '<p class="nota">' . $nota . '</p>';

                $puntuacion = get_field('puntuacion');

                if( $puntuacion > 0 && $puntuacion < 0.25 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 0.26 && $puntuacion < 0.75 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 0.76 && $puntuacion < 1.25 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 1.26 && $puntuacion < 1.75 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 1.76 && $puntuacion < 2.25 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 2.26 && $puntuacion < 2.75 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 2.76 && $puntuacion < 3.25 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 3.26 && $puntuacion < 3.75 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 3.76 && $puntuacion < 4.25 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="far fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 4.26 && $puntuacion < 4.75 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star-half-alt" aria-hidden="true"></i></li>';
                    echo '</ul>';

                } elseif ( $puntuacion > 4.76 && $puntuacion < 5 ) {

                echo '<ul class="stars">';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '<li><i class="fas fa-star" aria-hidden="true"></i></li>';
                    echo '</ul>';

                }

                echo '</div>';

                ?>

            </div>
        </aside>

<?php endif; ?>

	<footer class="entry-footer">
		<?php taurus_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->

</article><!-- #post-<?php the_ID(); ?> -->
