<?php
/**
 * Template part for displaying posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Taurus_theme
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <?php if ( !is_singular( 'page') ) : ?>

    <header class="entry-header">

        <div class="entry-excerpt">
	        <?php if( function_exists('the_powerpress_content')) : ?>

                <?php if ( is_singular('criticas-al-salir') ) :
			        $portada = content_url() . '/assets/portada_podcast-criticas-al-salir.jpg';
                elseif ( is_singular('cautivos-del-film') ) :
	                $portada = content_url() . '/assets/portada_podcast-cautivos-del-film.jpg';
                endif; ?>
                <img class="entry-excerpt-podcast" src="<?php echo $portada; ?>"/>

                <?php
                    if ( is_singular('criticas-al-salir') ) :
                        $EpisodeData = powerpress_get_enclosure_data( get_the_ID(), 'criticas-al-salir' );
                    elseif ( is_singular('cautivos-del-film') ) :
	                    $EpisodeData = powerpress_get_enclosure_data( get_the_ID(), 'cautivos-del-film' );
                    endif;
                $MediaURL = powerpress_add_flag_to_redirect_url( $EpisodeData['url'], '' );
                $download_link = powerpressplayer_link_download( '',$MediaURL, $EpisodeData );
		        $episode_content = get_the_powerpress_content();
		        ?>
                <div class="entry-excerpt-player">
                    <?php echo $episode_content ?>
                    <?php echo $download_link ?>
                </div>

	        <?php else : ?>
			    <?php the_excerpt(); ?>
            <?php endif; ?>
        </div><!-- .entry-meta -->

    </header><!-- .entry-header -->

    <?php endif; ?>

	<div class="entry-content">
		<?php

		if( is_singular( array( 'cautivos-del-film', 'criticas-al-salir', 'post' ) ) ) {

			if( function_exists('the_ratings')) {
				the_ratings();
			};

		}

			the_content( sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'taurus-theme' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			) );

			wp_link_pages( array(
				'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'taurus-theme' ),
				'after'  => '</div>',
			) );
		?>
    </div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php taurus_theme_entry_footer(); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-<?php the_ID(); ?> -->
