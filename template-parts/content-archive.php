<?php
/**
 * Template part for displaying archive
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Taurus_theme
 */
?>
<?php if( is_post_type_archive( array( 'criticas-al-salir', 'cautivos-del-film', 'doble-pletina' )) ): ?>
    <article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
        <a href="<?php the_permalink()?>" class="row-content">
            <div class="row-content-title">
                <h6>
                    <?php if ( is_post_type_archive('criticas-al-salir') ) : ?>
                        CAS<?php the_title()?>
		            <?php elseif ( is_post_type_archive('cautivos-del-film') ) : ?>
                        CDF<?php the_title()?>
                    <?php elseif ( is_post_type_archive('doble-pletina') ) : ?>
                        DPL<?php the_title()?>
                    <?php else : ?>
                        <?php the_title()?>
                    <?php endif; ?>
                </h6>
            </div>
            <div class="row-content-date">
                <span><?php the_date('d/m/Y')?></span>
            </div>
        </a>
    </article><!-- #post-<?php the_ID(); ?> -->
<?php elseif ( is_post_type_archive('criticas') ) : ?>
    <?php if ( $wp_query->post_count == 3 ) : ?>
    
	     <article id="post-ad-in-feed" class="box">

	     	<div class="box-content">
	            <div class="box-content-inner">
	            	<script async src="https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		    		<ins class="adsbygoogle"
		    		style="display:block"
		    		data-ad-format="fluid"
		    		data-ad-layout-key="+30+rf+1k-3j+55"
		    		data-ad-client="ca-pub-6276143273998710"
		    		data-ad-slot="9178744658"></ins>
		    		<script>
		    		(adsbygoogle = window.adsbygoogle || []).push({});
		    		</script>
	            </div>
        	</div>
	     
	     </article> 
    <?php else : ?>
    
    	<article id="post-<?php the_ID()?>" <?php post_class('box')?>>
	        <div class="box-image" style="
	                background-size: cover;
	                background-position: center;
	                background-image: url('<?php
			        if ( ! has_term( array('listas'), 'tipo' ) ) {
				        $movie_field = get_field('imdb');
	                    $movie = imdb_connector_get_movie($movie_field);
				        echo $movie['poster'];
			        } else {
				        the_post_thumbnail_url();
			        }
			        ?>');">
	        </div>
	        
			<div class="box-content">
	            <div class="box-content-inner">
	                <h2><?php the_title()?></h2>
	                <span><?php the_date()?></span>
	                <?php
	                    if ( has_term( 'alicantemag', 'tema' ) ) : 
	                        $post_id = get_the_ID();
	                        $canonical_url = WPSEO_Meta::get_value( 'canonical', $post_id );
	                    ?>
	                <a class="button" target="_blank" href="<?php echo $canonical_url; ?>">Leer</a> 
	                    <?php else : ?>
	                        <a class="button" href="<?php the_permalink()?>">Leer</a>
	                    <?php
	                    endif; ?>
	            </div>
	        </div>
    	</article><!-- #post-<?php the_ID(); ?> -->   
    <?php endif; ?>   
<?php else : ?>
        <article id="post-<?php the_ID(); ?>" <?php post_class('row'); ?>>
	        <a href="<?php the_permalink()?>" class="row-content">
	            <div class="row-content-title">
	                <h6><?php the_title()?></h6>
	            </div>
	            <div class="row-content-date">
	                <span><?php the_date('d/m/Y')?></span>
	            </div>
	        </a>
    	</article><!-- #post-<?php the_ID(); ?> -->
<?php endif; ?>